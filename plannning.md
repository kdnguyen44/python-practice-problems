# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value

Research:

What does pass? mean in the def function? -- > pass does nothing in particular but it is placed so that the code would run without getting an error

Problem Decomposition:

Finding the smaller number of the two numbers, if both numbers are the same then just print out one

SOLVED

### 04 max_of_three

Research:

Problem Decomposition:

Finding the greatest number out of three numbers, if two numbers are the same and they are the greatest just print out the one.

SOLVED

### 06 can_skydive

Research: You can return boolean statements like True or False.

Problem Decomposition:

Person needs to be >= to 18 OR person must have signed consent form.
has_consent_form=YES then it passes, if NO then it will not pass.

def can_skydive(age, has_consent_form):
    has_consent_form = "yes"
    if age >= 18:
        print("You're old enough and are allowed to go Skydiving! ")
    elif has_consent_form:
        print("You're not old enough, but you have a consent form so you can go! ")
    else:
        print("Sorry you can not skydive today.")

----- too complicated since I tried to add in print forms, you can just return True or False to pass

VIEWED SOLUTION BUT SOLVED AND UNDERSTOOD

### 09 is_palindrome

Research:

- reverse function --> multiple ways to reverse
- [::-1] --> all items in the array reversed -- example: word[::-1] - will reverse the word
- another way to do this would be to use reversed() -- it will reverse a seq
- .join() - joins elements and returns the combined string - need to use this to turn seq into argument


Problem Decomposition:

- first you have to reverse the word to get a new value
- use join function to create a new value
- compare the two values to make sure they are equal

SOLVED with research


### 010 is_divisible_by_3

Research: Return -- will return the output

Problem Decomposition:
- first we have to test to see if the number is divisible by 3 by using %3 == 0
- set output if it is and an else if not


SOLVED ON MY OWN BUT INSTEAD OF USING PRINT I SHOULD USE RETURN

### 011 is_divisible_by_5

SOLVED similar to problem_010

### 012 fizzbuzz

SOLVED combined both problems from 010 and 011

### 014 can_make_pasta

Research:

- list
- loop? maybe - nvm no loop needed
- you can use () in if statements

Problem Decomposition:

- need to format so the line of code is not too long

SOLVED just wrong formatting, need to clean up code

### 016 is_inside_bounds

Research:
- range? no need to use range here
- inclusive means to include 0 and 10 so I would need to use <= >= and not < > by itself

Problem Decomp:
- Take two numbers and make sure that the two numbers are between 0 and 10

SOLVED

### 019 is_inside_bounds

Research:

- I did have to fix the formatting to make it more standard

SOLVED -- just taking multiple if variables to return True

### 020 has_quorum

Research:
- len() - need to use this to get the number from lists

Problem Decomp:
- people in attendance >= 50% of the members list
- we need to figure out how many items are in the list first so we use len to figure out that number

SOLVED but my math equation was different than the solution -- attendees / members >= 0.5

### 022 gear_for_day

Research:

Problem Decomp:
- Returns a list of gear needed based on criteria
- If it is not sunny and workday --> list needs to contain "umbrella"
- If it is workday, the list needs to contain "laptop"
- If it is not a workday list needs to contain "surfboard"


### continue to plan each
