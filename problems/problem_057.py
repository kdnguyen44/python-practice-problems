# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4


#----not shown on solutions but more accurate to the wanted answer ---

def sum_fraction_sequence(num):
    list_num = list(range(1, num + 1))
    frac_list = []
    for i in list_num:
        frac = str(i) + "/" + str(i+1)
        frac_list.append(frac)
    new_list = " + ".join(frac_list)
    return new_list



#----Solutions---
# def sum_fraction_sequence(num):
#     sum = 0
#     for i in range(1, num + 1):
#         sum += i / (i + 1)
#     return sum

print(sum_fraction_sequence(3))
