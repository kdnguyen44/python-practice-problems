# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def can_skydive(age, has_consent_form):
#     has_consent_form = "yes"
#     if age >= 18:
#         print("You're old enough and are allowed to go Skydiving! ")
#     elif has_consent_form:
#         print("You're not old enough, but you have a consent!")
#     else:
#         print("Sorry you can not skydive today.")

# can_skydive(17, "no")

# ----correct answer


def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form:
        return True
    else:
        return False
