# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    attendees = len(attendees_list)
    members = len(members_list)
    if attendees >= members / 2:
        return True
    else:
        return False


test = has_quorum([1], [1, 2, 3, 4])
print(test)
